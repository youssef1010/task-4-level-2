package com.example.rockpaperscissors.Model

import android.os.Parcelable
import androidx.annotation.DrawableRes
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
@Entity(tableName="match_table")
data class Match (

    @ColumnInfo(name="ComputerChoice")
    @DrawableRes var computerChoice: Int,

    @ColumnInfo(name="yourChoice")
    @DrawableRes var playerChoice: Int,

    @ColumnInfo(name="win")
    var win:Boolean,

    @ColumnInfo(name="draw")
    var draw:Boolean,

    @ColumnInfo(name="loss")
    var loss:Boolean,


    @ColumnInfo(name = "date")
    var date : String,

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    var id: Long? = null

) : Parcelable
