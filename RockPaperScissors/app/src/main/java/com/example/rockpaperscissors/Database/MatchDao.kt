package com.example.rockpaperscissors.Database

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.example.rockpaperscissors.Model.Match

@Dao
interface MatchDao {

    @Query("SELECT * FROM match_table")
    suspend fun getAllMatch(): List<Match>

    @Query("SELECT COUNT(*) FROM match_table WHERE win = 1")
    suspend fun getAllWins(): Int

    @Query("SELECT COUNT(*) FROM match_table WHERE loss = 1")
    suspend fun getAllLosses(): Int

    @Query("SELECT COUNT(*) FROM match_table WHERE draw = 1")
    suspend fun getAllDraws(): Int

    @Insert
    suspend fun inserMatch(match: Match)

    @Delete
    suspend fun deleteMatch(match: Match)

    @Query("DELETE FROM match_table")
    suspend fun deleteAllMatches()

}

