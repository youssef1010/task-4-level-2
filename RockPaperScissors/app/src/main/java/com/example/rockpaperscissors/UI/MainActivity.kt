package com.example.rockpaperscissors.UI

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import com.example.rockpaperscissors.Database.MatchRepository
import com.example.rockpaperscissors.Model.Match
import com.example.rockpaperscissors.R
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.*


class MainActivity : AppCompatActivity() {

    private var wins:Int =0;
    private var draws:Int =0;
    private var losses:Int =0;

    private lateinit var matchRepository: MatchRepository
    private val mainScope = CoroutineScope(Dispatchers.Main)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        supportActionBar?.title = "Rock Paper Scissors Kotlin"

        matchRepository = MatchRepository(this)
        initViews()
        updateUI(null)



    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_delete -> {
                goToHistory()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun goToHistory(){

        startActivity(
            Intent(
                this@MainActivity,
                HistoryActivity::class.java
            )
        )
    }

    private fun initViews() {

        btnRock.setOnClickListener { addMatch(R.drawable.rock) }
        btnScissors.setOnClickListener { addMatch(R.drawable.scissors) }
        btnPaper.setOnClickListener { addMatch(R.drawable.paper) }
    }




    private fun addMatch(playerChoice: Int) {

        val match = Match(
            computerChoice = R.drawable.rock,
            playerChoice = playerChoice,
            date = Calendar.getInstance().time.toString(),
            win = false,
            loss = false,
            draw = false
        )

        val randomRoll: Int = (1..3).random()
        if (randomRoll == 1) {
            match.computerChoice = R.drawable.rock
            if (playerChoice == R.drawable.rock)
            match.draw = true
             else if (playerChoice == R.drawable.paper)
                match.win = true
             else match.loss = true
        }

        else if (randomRoll == 2) {
            match.computerChoice = R.drawable.paper
            if (playerChoice == R.drawable.rock)
                match.loss = true
            else if (playerChoice == R.drawable.paper)
                match.draw = true
            else match.win = true
        }

        else {
            match.computerChoice = R.drawable.scissors
            if (playerChoice == R.drawable.rock)
                match.win = true
            else if (playerChoice == R.drawable.paper)
                match.loss = true
            else match.draw = true
        }


        CoroutineScope(Dispatchers.Main).launch {
            withContext(Dispatchers.IO) {
                matchRepository.insertProduct(match)
            }
        }
        updateUI(match)

}
    private fun updateUI(match:Match?) {

        if(match!=null) {
            ivComputer.setImageResource(match.computerChoice);
            ivYou.setImageResource(match.playerChoice);
            if (match.win)
                tvResult.text = "You win!"
            if (match.draw)
                tvResult.text = "Draw!"
            if (match.loss)
                tvResult.text = "You lose!"
        }

        CoroutineScope(Dispatchers.Main).launch {
            withContext(Dispatchers.IO) {
                wins = matchRepository.getAllWins()
                draws = matchRepository.getAllDraws()
                losses = matchRepository.getAllLosses()
            }
        }

        tvScore.text="Wins: $wins Draws: $draws Losses: $losses"



    }
}
