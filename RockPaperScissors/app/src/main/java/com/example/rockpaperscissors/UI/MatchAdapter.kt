package com.example.rockpaperscissors.UI

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.DrawableRes
import androidx.recyclerview.widget.RecyclerView
import com.example.rockpaperscissors.Model.Match
import com.example.rockpaperscissors.R
import kotlinx.android.synthetic.main.activity_main.view.*
import kotlinx.android.synthetic.main.item_history.view.*

public class  MatchAdapter(private val matches: List<Match>) :
    RecyclerView.Adapter<MatchAdapter.ViewHolder>() {

    lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_history, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return matches.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(matches[position])
    }



    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(match : Match) {

//            if(playerChoice == R.drawable.rock)
//                match.playerChoice=1;
//            if(playerChoice == R.drawable.rock)
//                match.playerChoice=1;
//
//            itemView.ivHistoryComputer.setImageResource(intToDrawable(match.computerChoice))
//            itemView.ivHistoryPlayer.setImageResource(intToDrawable(match.playerChoice))
            itemView.ivHistoryComputer.setImageResource(match.computerChoice)
            itemView.ivHistoryPlayer.setImageResource(match.playerChoice)

            if(match.draw)
                itemView.tvWinner.text="Draw!"
            if(match.win)
                itemView.tvWinner.text="You win!"
            if(match.loss)
                itemView.tvWinner.text="You lose"

            itemView.tvDate.text=match.date
        }
    }
}
