package com.example.rockpaperscissors.Database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.rockpaperscissors.Model.Match

@Database(entities = [Match::class], version = 1, exportSchema = false)
abstract class MatchClassRoomDatabase : RoomDatabase() {

    abstract fun matchDao(): MatchDao

    companion object {
        private const val DATABASE_NAME = "SHOPPING_LIST_DATABASE"

        @Volatile
        private var MatchClassRoomDatabaseInstance: MatchClassRoomDatabase? = null

        fun getDatabase(context: Context): MatchClassRoomDatabase? {
            if (MatchClassRoomDatabaseInstance == null) {
                synchronized(MatchClassRoomDatabase::class.java) {
                    if (MatchClassRoomDatabaseInstance == null) {
                        MatchClassRoomDatabaseInstance =
                            Room.databaseBuilder(context.applicationContext,
                                MatchClassRoomDatabase::class.java, DATABASE_NAME).build()
                    }
                }
            }
            return MatchClassRoomDatabaseInstance
        }
    }

}
