package com.example.rockpaperscissors.Database

import android.content.Context
import com.example.rockpaperscissors.Model.Match

class MatchRepository(context: Context) {

    private val matchDao: MatchDao

    init {
        val database = MatchClassRoomDatabase.getDatabase(context)
        matchDao = database!!.matchDao()
    }

    suspend fun getAllProducts(): List<Match> {
        return matchDao.getAllMatch()
    }

    suspend fun insertProduct(match: Match) {
        matchDao.inserMatch(match)
    }

    suspend fun getAllWins() :Int{
        return matchDao.getAllWins()
    }

    suspend fun getAllLosses() :Int{
        return matchDao.getAllLosses()
    }

    suspend fun getAllDraws() :Int{
        return matchDao.getAllDraws()
    }

    suspend fun deleteProduct(match: Match) {
        matchDao.deleteMatch(match)
    }
    suspend fun deleteAllMatches() {
        matchDao.deleteAllMatches()
    }

}
