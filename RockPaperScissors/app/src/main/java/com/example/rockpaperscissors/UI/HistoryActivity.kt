package com.example.rockpaperscissors.UI

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.example.rockpaperscissors.Database.MatchDao
import com.example.rockpaperscissors.Database.MatchRepository
import com.example.rockpaperscissors.Model.Match
import com.example.rockpaperscissors.R
import kotlinx.android.synthetic.main.activity_history.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class HistoryActivity : AppCompatActivity() {


    private lateinit var matchRepository: MatchRepository
    private val mainScope = CoroutineScope(Dispatchers.Main)
    private var matches = arrayListOf<Match>()
    private val matchAdapter = MatchAdapter(matches)


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_history)
        setSupportActionBar(toolbar4)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = "This is your history!"
        matchRepository = MatchRepository(this)
        initViews()
    }
    private fun initViews() {

//        if (::matchRepository.isInitialized)
        getMatchesFromDatabase()
        rvMatches.layoutManager = StaggeredGridLayoutManager(1, LinearLayoutManager.VERTICAL)
        rvMatches.adapter = matchAdapter
        matchAdapter.notifyDataSetChanged()

    }
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_activity, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            R.id.action_delete -> {
                CoroutineScope(Dispatchers.Main).launch {
                    withContext(Dispatchers.IO) {
                        matchRepository.deleteAllMatches()
                    }
                }
                true
            }
            else -> return super.onOptionsItemSelected(item)
        }
        this@HistoryActivity.matchAdapter.notifyDataSetChanged()
        matchAdapter.notifyDataSetChanged()
    }

    private fun getMatchesFromDatabase() {
        mainScope.launch {
            val shoppingList = withContext(Dispatchers.IO) {
                matchRepository.getAllProducts()
            }
            this@HistoryActivity.matches.clear()
            this@HistoryActivity.matches.addAll(shoppingList)
            this@HistoryActivity.matchAdapter.notifyDataSetChanged()

        }
    }
//    private suspend fun deleteHistory(){
//        CoroutineScope(Dispatchers.Main).launch {
//            withContext(Dispatchers.IO) {
//                matchRepository2.deleteAllMatches()
//            }
//        }
//
//    }

}